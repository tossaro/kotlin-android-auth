@file:Suppress("UNUSED")

package multi.platform.auth.shared.external.enums

enum class UserState {
    VALIDATED, SECURED, REGISTERED, ACTIVATED, UNDEFINED
}

@file:Suppress("UNUSED")

package multi.platform.auth.shared.external.enums

enum class AuthType {
    PHONE, EMAIL, GOOGLE, FACEBOOK, BIOMETRIC, UNDEFINED
}

package multi.platform.auth.shared.domain.auth.entity

@Suppress("UnUsed")
data class CountryCode(
    val code: String,
    val dialCode: String,
    val countryName: String,
)

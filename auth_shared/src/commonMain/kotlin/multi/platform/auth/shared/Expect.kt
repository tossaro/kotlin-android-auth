package multi.platform.auth.shared

import org.koin.core.module.Module

expect fun authModule(): Module

package multi.platform.auth.shared.external.constants

object AuthKey {
    const val SIGN_IN_KEY = "sign-in"
    const val OTP_KEY = "otp"
    const val REGISTER_KEY = "register"
    const val VERIFY_KEY = "verify"
    const val COUNTRY_KEY = "country"
    const val TRANSACTION_ID = "trxId"
    const val SIGN_OUT_KEY = "sign-out"
    const val FORGET_PASSWORD_KEY = "forget-password"
}
